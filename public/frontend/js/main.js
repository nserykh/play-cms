jQuery(document).ready(function($) {
    $('.flexslider').flexslider({
        animation: "slide"
    });

    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });

    $('.editbar').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });
});