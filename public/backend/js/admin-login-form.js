(function($) {
    $(document).on('submit', 'form#admin-login-form', function() {
        form = $(this);
        var container = $('div.content');
        container.fadeOut({
            'duration' : 200,
            'done' : function() {
                $('#login-error', form).addClass("hide");
                $('#group-email').removeClass("has-error");
                $('#help-email').addClass("hide").html("");
                $('#group-password').removeClass("has-error");
                $('#help-password').addClass("hide").html("");
	            $.ajax({
		            type: "POST",
		            url: form.attr('action'),
		            data: form.serialize(),
		            dataType : 'json',
		            xhrFields: { withCredentials: true },
                    success : function(response) {
                    	if (response.status === 'redirect') {
                            window.location.href = response.content;
                        } else if (response.status === 'error') {
                            $('input[name=password]').val("");
                            $('#login-error', form).removeClass("hide");
                            if(response.errors) {
                                for(index in response.errors) {
                                    $("#group-"+index).addClass("has-error");
                                    $("#help-"+index).html(response.errors[index].join("<br />"));
                                    $("#help-"+index).removeClass("hide");
                                }
                            }
                            container.fadeIn({
                                'duration' : 200
                            });
                        } else {
                        	window.location.href = jsRoutes.controllers.Administration.index().url;
                        }
                    }
                });
            }
        })
        return false;
    });
})(jQuery)