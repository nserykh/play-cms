/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.cms.attributeset.views.html.backend._optionAttributeForm;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Result;
import play.twirl.api.Html;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="cms_eav_attribute_option")
@DiscriminatorValue("option")
public class OptionAttribute extends Attribute {

    private final static Logger logger = LoggerFactory.getLogger(OptionAttribute.class);

    @OneToMany(mappedBy = "parentAttribute", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn
    private List<Option> options = new ArrayList<>();

    private Integer multiplicity;

    public boolean isSingleSelect(){
        return getMultiplicity() == 1;
    }

    @Override
    public Html editForm(play.data.Form form, Value value) {
        return ch.insign.cms.attributeset.views.html.backend.optionAttributeForm.render(form, this, (OptionValue)value);
    }

    @Override
    public Value getDefaultValue() {
        return new OptionValue();
    }

    @Override
    public Html editAttributeForm(play.data.Form form) {
        return _optionAttributeForm.render(form);
    }

    @Override
    public Value restoreValueFromRequest(Value value) {
        OptionValue optionValue = Optional.ofNullable(value)
                .filter(Objects::nonNull)
                .map(val -> (OptionValue) val)
                .orElse(new OptionValue());

        // Add new values to option
        optionValue.setValue(new ArrayList<>());
        Map<String, String> formData = DynamicForm.form().bindFromRequest().data();
        for (int i = 0; ; i++) {
            String optionId = formData.get("setInstance." + getKey() + ".value[" + i + "]");

            if (optionId == null || optionId.isEmpty()) {
                break;
            }

            Option option = Option.find.byId(optionId);
            optionValue.addValue(option);
            option.addMappedValue(optionValue);
        }

        // If value doesn't exists in the form (request) - delete it from db
        if (optionValue.getValue().isEmpty()) {
            if (optionValue.getId() != null) {
                optionValue.delete();
            }
            return null;
        }

        /*
         Persist new (not empty) OptionValue to avoid the following error:
         "During synchronization a new object was found through a relationship
         that was not marked cascade PERSIST: OptionValue (null)"

         It would be  better to add "cascade = CascadeType.ALL" for attributeset.model.Option#mappedValues
         but updating a Map<Entity, Entity> is working only partially.
         Changes are committed in database, but the entity remain the same as before the update.
         The following error appears: "Element [OptionValue (5555)] is being added to a map without a key.
         This generally means the database does not hold a key that is expected."
         See https://bugs.eclipse.org/bugs/show_bug.cgi?id=418031
        */
        if (optionValue.getId() == null && !optionValue.getValue().isEmpty()) {
            jpaApi.em().persist(optionValue);
        }

        return optionValue;
    }

    @Override
    public Result doEdit() {
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        HashMap<String, Option> map = buildOptionMap(getOptions(), new HashMap<>());
        NestedObject[] newOrder = new Gson().fromJson(dynamicForm.get("navorder"), NestedObject[].class);
        options = new ArrayList<>();

        for (NestedObject obj: newOrder){
            Option option = map.get(obj.id);
            options.add(option);
            option.setParentAttribute(this);
            traverseNestedOption(option, obj.children, map);
        }
        return super.doEdit();
    }

    private static class NestedObject {
        String id;
        NestedObject[] children;
    }

    private HashMap<String, Option> buildOptionMap(List<Option> list, HashMap<String, Option> map) {
        if (list == null)
            return map;
        for (Option option: list){
            map.put(option.getId(), option);
            option.setParentAttribute(null);
            buildOptionMap(option.getChildren(), map);
            option.getChildren().clear();
        }
        return map;
    }

    private void traverseNestedOption(Option option, NestedObject[] children, HashMap<String, Option> map){
        if (children == null)
            return;
        for (NestedObject child: children){
            Option next = map.get(child.id);
            option.getChildren().add(next);
            next.setParentOption(option);
            traverseNestedOption(next, child.children, map);
        }
    }

    // ====== Getter and Setter ======

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Integer getMultiplicity() {
        return multiplicity == null ? 0 : multiplicity;
    }

    public void setMultiplicity(Integer multiplicity) {
        this.multiplicity = multiplicity;
    }
}
