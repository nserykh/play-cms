/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset;

import ch.insign.cms.attributeset.model.*;
import ch.insign.commons.db.Model;
import play.data.DynamicForm;
import play.data.Form;
import play.twirl.api.Html;

import java.util.HashSet;
import java.util.Set;


/**
 * A service class that provides all the functionality of saving, retrieving and rendering attributes
 * and the attribute values for a particular entity.
 *
 * All the attributes are saved completely outside of the entity that "owns" them, which means that you
 * don't need to define any additional properties or methods in your entity. You only need to specify a key
 * that will uniquely identify your entity. Please note, that this ID must be unique not only for a given
 * class, but for ALL ENTITIES that use attributes.
 */

// Yes, the methods are static. Yes, that's not cool. However, we have no DI in scala templates,
// so this is the lesser evil. -vz
public class AttributeService {

    /**
     * Use this method to render an additional Html in your backend form, containing the attribute set
     * belonging to an entity.
     * @param form The populated form, used for rendering in the backend.
     * @param attributeSet The key of the attribute set, that will be rendered.
     * @param entityKey The key of the entity being edited.
     * @return Html with the additional form content.
     */
    public static Html renderForm(Form form, String attributeSet, String entityKey){
        AttributeSet set = AttributeSet.find.byKey(attributeSet);
        if (set != null)
            return ch.insign.cms.attributeset.views.html.backend.attributeSet.render(form, set, entityKey);
        else
            return new Html("");
    }

    /**
     * This method saves an {@link AttributeSetInstance}. Since attributes may belong to multiple attribute sets
     * the method requires not only the key of the owning entity, but also of the corresponding attribute set. The
     * attribute set is passed as a hidden field in the form and is read from the currently executed request.
     * The hidden field is appended to your form automatically, if you use the
     * {@link #renderForm(Form, String, String)} to render the attribute set in the backend form.
     * @param entityKey The key of the entity, to which the instance belongs.
     */
    public static void save(String entityKey) {
        // FIXME: Binding form in save method is an ugly solution
        // It relies on Http Context which may not exists here
        if (play.mvc.Http.Context.current.get() == null) {
            return;
        }

        DynamicForm form = DynamicForm.form().bindFromRequest();
        String key = form.get("_attributeSet.key");
        if (key == null){
            return;
        }
        AttributeSet set = AttributeSet.find.byKey(key);
        if (set == null){
            return;
        }

        AttributeSetInstance instance = set.getAttributeSetInstance(entityKey);
        if (instance == null) {
            instance = new AttributeSetInstance();
            instance.setOwnerObject(entityKey);
        }

        for (Attribute attr: set.getAttributes()){
            Value val = attr.restoreValueFromRequest(instance.get(attr));

            if (val == null) {
                instance.remove(attr);
                continue;
            }

            val.setInstance(instance);
            instance.put(attr, val);
        }

        set.getSelections().put(entityKey, instance);
        instance.setSet(set);
        instance.save();
    }

    /**
     *
     * @return The {@link Value} saved for a particular entity for a particular attribute in a particular
     * attribute set.
     */
    public static Value getAttributeValue(String attrSet, String attrKey, String entityKey) {
        AttributeSet set = AttributeSet.find.byKey(attrSet);
        if (set == null) {
            return null;
        }

        AttributeSetInstance instance = set.getSelections().get(entityKey);
        if (instance == null) {
            return null;
        }

        Attribute attribute = Attribute.find.byKey(attrKey);
        if (attribute == null) {
            return null;
        }

        if (!set.getAttributes().contains(attribute)) {
            return null;
        }

        return instance.get(attribute);
    }

    /**
     * Used as a Builder-Pattern for search of objects by a list of criteria based on attributes.
     * See {@link AttributeSearch} for reference.
     */
    public static <T extends Model> AttributeSearch<T> search(Model.Finder<T> finder){
        return new AttributeSearch<>(finder);
    }

    /**
     * Find all options from an OptionAttribute that are assigned to at least one object of the type
     * specified by the given finder.
     *
     * Note on performance: this method can/will fire multiple queries. This may be fast on some occasions
     * (depending on JPA cache, user behaviour and other non-deterministic criteria), but it may also be
     * SIGNIFICANTLY slower than a single query with a bunch of joins. As with other methods in this class,
     * please monitor performance and optimize if necessary!
     */
    public static <T extends Model> Set<Option> findDistinctOptions(Model.Finder<T> finder, String setKey,
                                                                    String attrKey){
        Set<Option> res = new HashSet<>();
        for (T elem: finder.all()){
            OptionValue val = (OptionValue)getAttributeValue(setKey, attrKey, elem.getId());
            if (val != null) {
                for (Option option: val.getValue()){
                    res.add(option);
                }
            }
        }
        return res;
    }
}