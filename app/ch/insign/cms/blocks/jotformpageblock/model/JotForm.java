/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock.model;

import ch.insign.cms.blocks.jotformpageblock.JotFormPageBlock;
import play.db.jpa.JPA;
import ch.insign.commons.db.Model;
import ch.insign.commons.util.Configuration;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model stores imported from the jot form's api forms data.
 **/
@Entity
@Table(name = "cms_jotform")
@NamedQueries(value = {
        @NamedQuery(
                name = "JotForm.findByApiFormId",
                query = "SELECT j FROM JotForm j WHERE j.apiFormId = :apiFormId AND j.jotFormApiKey = :jotFormApiKey")
})
public class JotForm extends Model implements Serializable {

    public static JotFormFinder find = new JotFormFinder();

    /**
     * Id form, from the remote builder
     */
    @Constraints.Required
    private Long apiFormId;

    private String jotFormApiKey;

    /**
     * Date of creation of the form, from the remote builder
     */
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apiFormCreate;

    /**
     * Title form, from the remote builder
     */
    private String apiFormTitle;

    public JotForm() {
    }

    /**
     * Constructor to create a form that will be used for the JotFormPageBlock
     * It includes minimum list of fotForm api fields, needed to use in project
     *
     * @param apiFormId     - id from jotForm api
     * @param apiFormCreate - create date from jotForm api
     * @param apiFormTitle  - title from jotForm api
     * @param jotFormApiKey - api key from jotForm account
     */
    public JotForm(Long apiFormId, Date apiFormCreate, String apiFormTitle, String jotFormApiKey) {
        this.apiFormId = apiFormId;
        this.apiFormCreate = apiFormCreate;
        this.apiFormTitle = apiFormTitle;
        this.jotFormApiKey = jotFormApiKey;
    }

    public String getJotFormApiKey() {
        return jotFormApiKey;
    }

    public void setJotFormApiKey(String jotFormApiKey) {
        this.jotFormApiKey = jotFormApiKey;
    }

    public String getApiFormTitle() {
        return apiFormTitle;
    }

    public void setApiFormTitle(String apiFormTitle) {
        this.apiFormTitle = apiFormTitle;
    }

    public Date getCreate() {
        return apiFormCreate;
    }

    public void setCreate(Date apiFormCreate) {
        this.apiFormCreate = apiFormCreate;
    }

    public Long getApiFormId() {
        return apiFormId;
    }

    public void setApiFormId(Long apiFormId) {
        this.apiFormId = apiFormId;
    }

    public static class JotFormFinder extends Model.Finder<JotForm> {
        private JotFormFinder() {
            super(JotForm.class);
        }

        public JotForm byUid(Long apiFormId) {
            return jpaApi.em().createNamedQuery("JotForm.findByApiFormId", JotForm.class)
                    .setParameter("apiFormId", apiFormId)
                    .setParameter("jotFormApiKey", Configuration.getOrElse("jotform.api.key", ""))
                    .getResultList().stream().findFirst().orElse(null);
        }
    }
}
