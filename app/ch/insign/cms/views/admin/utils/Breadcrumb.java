/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.views.admin.utils;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
@Deprecated
public class Breadcrumb  {
	private final static Logger logger = LoggerFactory.getLogger(Breadcrumb.class);

    private String name;

    private String url;

    public Breadcrumb(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

}
