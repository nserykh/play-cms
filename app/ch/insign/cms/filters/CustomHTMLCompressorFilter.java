/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.filters;

import akka.stream.Materializer;
import com.google.inject.Inject;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.mohiva.play.htmlcompressor.HTMLCompressorFilter;
import play.api.Environment;

public class CustomHTMLCompressorFilter extends HTMLCompressorFilter {

    private play.api.Configuration configuration;
    private Environment environment;
    private Materializer mat;

    @Inject
    public CustomHTMLCompressorFilter(
            play.api.Configuration configuration, Environment environment, Materializer mat) {

        this.configuration = configuration;
        this.environment = environment;
        this.mat = mat;
    }

    @Override
    public HtmlCompressor compressor() {
        HtmlCompressor compressor = new HtmlCompressor();
        compressor.setPreserveLineBreaks(true);
        compressor.setRemoveComments(true);
        compressor.setCompressCss(true);
        compressor.setCompressJavaScript(true);
        compressor.setRemoveMultiSpaces(true);
        compressor.setRemoveIntertagSpaces(true);
        compressor.setRemoveHttpProtocol(false);
        compressor.setRemoveHttpsProtocol(false);
        return compressor;
    }

    @Override
    public play.api.Configuration configuration() {
        return configuration;
    }

    @Override
    public Materializer mat() {
        return mat;
    }
}
