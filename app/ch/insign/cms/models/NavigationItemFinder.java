/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.Model.Finder;
import play.db.jpa.JPA;

import javax.persistence.TypedQuery;
import java.util.List;

public class NavigationItemFinder<T extends NavigationItem> extends Finder<T>{

    // FIXME: All need the site param now..

    public NavigationItemFinder(Class<T> tClass) {
        super(tClass);
    }

    public T byVpath(String vpath) {
        return byVpath(vpath, CMS.getSites().current().key);
    }

    public T byVpath(String vpath, String siteKey) {
        return JPA.em().createNamedQuery("NavigationItem.findByVpath", getEntityClass())
                .setParameter("vpath", vpath)
                .setParameter("site", siteKey)
                .getResultList().stream().findFirst().orElse(null);
    }

    public T byVpathHistory(String vpath) {
        return JPA.em().createNamedQuery("NavigationItem.findByVpathHistory", getEntityClass())
                .setParameter("vpath", vpath)
                .setParameter("site", CMS.getSites().current().key)
                .getResultList().stream().findFirst().orElse(null);
    }

    public List<T> byVpathOrVpathHistory(String vpath, String site) {
        return queryByVpathOrVpathHistory(vpath, site).getResultList();
    }

    public TypedQuery<T> queryByVpathOrVpathHistory(String vpath, String site) {
        return JPA.em().createNamedQuery("NavigationItem.findByVpathOrVpathHistory", getEntityClass())
                .setParameter("vpath", vpath)
                .setParameter("site", site);
    }

    public long countByVpathOrVpathHistory(String vpath, String site) {
        return JPA.em().createNamedQuery("NavigationItem.findByVpathOrVpathHistory.count", Long.class)
                .setParameter("vpath", vpath)
                .setParameter("site", site)
                .getSingleResult();
    }

}
