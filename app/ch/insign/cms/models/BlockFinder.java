/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.Model.Finder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BlockFinder<T extends AbstractBlock> extends Finder<T> {
	private final static Logger logger = LoggerFactory.getLogger(BlockFinder.class);

	@Inject
	protected static JPAApi jpaApi;

	/** The single root parent node which never changes **/
	private static AbstractBlock rootParent;

	/** The single trash parent node which never changes **/
	private static AbstractBlock trashParent;

	public BlockFinder(Class<T> entity) {
		super(entity);
	}

	@Nullable
	public T byKey(String key) {
		try {
			String siteKey = CMS.getSites().current().key;

			return byKey(key, siteKey);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Nullable
	public T byKey(String blockKey, String siteKey) {
		List<T> results = jpaApi.em()
				.createNamedQuery("Block.findByKey", getEntityClass())
				.setParameter("key", blockKey)
				.setParameter("site", siteKey)
				.getResultList();

		if (results.size() == 1) {
			return results.get(0);
		} else if (results.size() > 1) {
			// If there is a matching block for the specified site and one for all sites (wildcard site),
			// take the one for the matched site
			logger.warn("Found multiple blocks matching key '{}' and site '{}' (wildcard and current host?)", blockKey, siteKey);
			return results.stream()
					.filter(b -> b.getSite().equals(siteKey))
					.findFirst()
					.orElse(null);
		} else {
			return null;
		}
	}

	public List<T> allBySite(String siteKey) {
		try {
			List<T> results = jpaApi.em()
					.createNamedQuery("Block.findAllBySite", getEntityClass())
					.setParameter("site", siteKey)
					.getResultList();

			return results;
		} catch (Exception e) {
			logger.error("Error loading blocks by siteKey", e);
			return new ArrayList<>();
		}
	}

	@Nullable
	public T byParentAndSlot(AbstractBlock parentBlock, String slot) {

		try {
			String siteKey = parentBlock.getSite();

			List<T> results = jpaApi.em()
				.createNamedQuery("Block.findByParentAndSlot", getEntityClass())
				.setParameter("parentBlock", parentBlock)
				.setParameter("slot", slot)
				.setParameter("site", siteKey)
				.getResultList();

			// If there is a matching block for the current site and one for all sites (wildcard site), take the one for the current site
			if (results.size() == 1) {
				return results.get(0);
			}
			else if (results.size() > 1) {
				logger.warn("Found multiple blocks matching parentBlock id {}, slot '{}' and site '{}' (wildcard and current host?)", parentBlock.getId(), slot, siteKey);
				return results.stream()
					.filter(b -> b.getSite().equals(CMS.getSites().current().key))
					.findFirst().orElse(null);
			}
			else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<T> byTypeAndDate(String blockType, Date from, Date to, Integer limit) {
		try {
			CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
			CriteriaQuery criteria = builder.createQuery(AbstractBlock.class);
			Root entityRoot = criteria.from(AbstractBlock.class);
			Root<PageBlock> pageBlockRoot = builder.treat(entityRoot, PageBlock.class);

			List<Predicate> predicates = new ArrayList<>();

			predicates.add(builder.equal(entityRoot.get("block_type"), blockType));
			predicates.add(builder.equal(entityRoot.get("site"), CMS.getSites().current().key));

			if (from != null) {
				predicates.add(builder.or(
						builder.greaterThanOrEqualTo(pageBlockRoot.get("publishDate"), from),
						builder.isNull(pageBlockRoot.get("publishDate"))));
			}

			if (to != null) {
				predicates.add(builder.or(
						builder.lessThanOrEqualTo(pageBlockRoot.get("publishDate"), to),
						builder.isNull(pageBlockRoot.get("publishDate"))));
			}

			criteria.select(entityRoot).where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
			criteria.orderBy(builder.desc(pageBlockRoot.get("publishDate")));

			TypedQuery<T> query = jpaApi.em().createQuery(criteria);
			if (limit != null) {
				query.setMaxResults(limit);
			}
			return query.getResultList();
		} catch (Exception e) {
			logger.error("Error loading blocks by type and date", e);
			return new ArrayList<>();
		}
	}

	/**
	 * Get all root page blocks (that have the root node as parent block)
	 */
	public List<PageBlock> rootNodes(boolean allSites) {

		List<PageBlock> result = new ArrayList<>();
		for (AbstractBlock node : byKey(PageBlock.KEY_ROOT).getSubBlocks()) {
			if (node instanceof PageBlock) {

				// Get only nodes that match this site (same site or wildcard site)
				if (allSites || (CMS.getSites().current().key.equals(node.getSite()) || "*".equals(node.getSite()))) {
					result.add((PageBlock) node);
				}

			} else {
				logger.warn("Block " + node + " is attached to root node but is not a PageBlock. Not good.");
			}
		}
		return result;
	}


	/**
	 * Get the trash page node. All subblocks of this node are considered
	 * trash bin items. Any subblocks deleted from here are permanently deleted
	 */
	@Nullable
	public AbstractBlock trash() {
		if (trashParent == null) {
			trashParent = byKey(PageBlock.KEY_TRASH);
		}

		if (trashParent ==  null) {
			logger.error("Could not retrieve the parent trash node (a block with key " + PageBlock.KEY_TRASH + ")");
		}
		return trashParent;
	}

	/**
	 * Get the drafts page node. The user can push pages to the drafts node to
	 * temporarily keep them away from publically visible nodes.
	 */
	public AbstractBlock drafts() {
		return trash();
	}

	/**
	 * Get the frontend grouping node. All subblocks are supposed to be frontend website blocks.
	 */
	@Nullable
	public T frontend() {
		T frontend = byKey(PageBlock.KEY_FRONTEND);
		if (frontend ==  null) {
			logger.error("Could not retrieve the frontend root node (a block with key " + PageBlock.KEY_FRONTEND + ")");
		}
		return frontend;
	}

	/**
	 * Get the backend grouping node. All subblocks are supposed to be admin backend pages.
	 */
	@Nullable
	public T backend() {
		T backend = byKey(PageBlock.KEY_BACKEND);
		if (backend ==  null) {
			logger.error("Could not retrieve the backend root node (a block with key " + PageBlock.KEY_BACKEND + ")");
		}
		return backend;
	}

	/**
	 * Get the site grouping node. All subblocks are supposed to belong to this site oly.
	 */
	@Nullable
	public T site() {
		T site = byKey(PageBlock.KEY_SITE);
		if (site ==  null) {
			logger.warn("Could not retrieve a site root node for site '{}'. Site not initialized yet?", CMS.getSites().current().name);
		}
		return site;
	}

	/**
	 * Get the root node's (invisible) parent root node. This node is the parent of the
	 * top-level root nodes. Set it as parent to the root nodes.
	 *
	 * This node's the only one allowed to have a null parent.
	 */
	public AbstractBlock rootParent() {
		return rootParent(false);
	}

	/**
	 * Optionally silent version - package use only.
	 * @param silent
	 */
	@Nullable
	AbstractBlock rootParent(boolean silent) {
		if (rootParent == null) {
			rootParent = byKey(PageBlock.KEY_ROOT);
		}

		if (rootParent ==  null && !silent) {
			logger.error("Could not retrieve the parent root node (a block with key " + PageBlock.KEY_ROOT + ")");
		}
		return rootParent;
	}

	/**
	 * Flush any cached state (for now only rootParent).
	 */
	public static void flush() {
		rootParent = null;
	}

}
