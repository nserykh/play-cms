/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import java.util.Collection;
import java.util.Optional;

public interface AccessControlListVoter {

	/**
	 * Optionally returns a vote for the given authorization request.
	 */
	Optional<Vote> vote(Object authority, DomainPermission<?> permission);

	/**
	 * Optionally returns an inherited vote on the given permission for a single SecurityIdentity.
	 */
	Optional<Vote> inheritedVote(SecurityIdentity authority, DomainPermission<?> permission);

	/**
	 * Optionally reduces multiple votes into a single vote.
	 */
	Optional<Vote> reduce(Collection<Vote> votes);

	/**
	 * The Vote describes the response on the authorization request.
	 */
	class Vote {
		private final SecurityIdentity sid;
		private final DomainPermission<?> permission;
		private final boolean allowed;

		public Vote(AccessControlEntry ace, DomainPermission<?> permission, boolean allowed) {
			this.sid = ace.getSid();
			this.permission = permission;
			this.allowed = allowed;
		}

		public SecurityIdentity sid() {
			return sid;
		}

		public ObjectIdentity oid() {
			return permission.target();
		}

		public DomainPermission<?> permission() {
			return permission;
		}

		public boolean allowed() {
			return allowed;
		}

		public boolean denied() {
			return !allowed;
		}
	}
}
