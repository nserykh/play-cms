/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import ch.insign.playauth.permissions.GlobalDomainPermission;
import org.apache.commons.lang3.Validate;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DomainActionEntry implements Serializable {

	public static final DomainActionEntry ALL = new DomainActionEntry(GlobalDomainPermission.ALL.domain(), GlobalDomainPermission.ALL.name());

	private String domain;
	private String name;

	protected DomainActionEntry() { }

	public DomainActionEntry(DomainPermission<?> permission) {
		this(permission.domain(), permission.name());
	}

	public DomainActionEntry(String domain, String name) {
		Validate.notBlank(domain);
		Validate.notBlank(name);

		this.domain = domain;
		this.name = domain.equals(GlobalDomainPermission.ALL.domain()) ? GlobalDomainPermission.ALL.name() : name;
	}

	public String domain() {
		return domain;
	}

	public String name() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DomainActionEntry)) return false;

		DomainActionEntry that = (DomainActionEntry) o;

		if (!domain.equals(that.domain)) return false;
		if (!name.equals(that.name)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = domain.hashCode();
		result = 31 * result + name.hashCode();
		return result;
	}
}
