/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Map;

import org.apache.shiro.session.mgt.DefaultSessionContext;

import play.mvc.Http;


public class DefaultPlayShiroSessionContext extends DefaultSessionContext implements PlayShiroSessionContext {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSessionContext.class);

    private static final long serialVersionUID = 4546076386206737829L;

    private static final String HTTP_CONTEXT = DefaultPlayShiroSessionContext.class.getName() + ".HTTP_CONTEXT";

    public DefaultPlayShiroSessionContext() {
        super();
    }

    public DefaultPlayShiroSessionContext(Map<String, Object> map) {
        super(map);
    }

    @Override
    public void setHttpContext(Http.Context context) {
        if (context != null) {
            put(HTTP_CONTEXT, context);
        }
    }

    @Override
    public Http.Context getHttpContext() {
        return getTypedValue(HTTP_CONTEXT, Http.Context.class);
    }
}
