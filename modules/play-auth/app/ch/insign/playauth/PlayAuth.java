/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyManager;
import ch.insign.playauth.party.PartyRoleManager;
import ch.insign.playauth.shiro.PlayShiroApi;
import com.google.inject.Inject;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Provides access to authentication and authorization services.
 *
 * @deprecated inject ch.insign.playauth.PlayAuthApi instead.
 */
@Deprecated
public class PlayAuth {

	private final static Logger logger = LoggerFactory.getLogger(PlayAuth.class);

	@Inject
	private static PlayAuthApi api;

	public static PlayShiroApi env() {
		return api.env();
	}

    public static EventDispatcher getEventDispatcher() {
        return api.getEventDispatcher();
    }

	public static AccessControlList getAccessControlList() {
		return api.getAccessControlList();
	}

	public static Authorizer getAuthorizer() {
		return api.getAuthorizer();
	}

	public static AccessControlListVoter getAccessControlListVoter() {
		return api.getAccessControlListVoter();
	}

    public static AccessControlManager getAccessControlManager() {
		return api.getAccessControlManager();
    }

    public static PermissionManager getPermissionManager() {
		return api.getPermissionManager();
    }

    public static PartyManager getPartyManager() {
		return api.getPartyManager();
    }

    public static PartyRoleManager getPartyRoleManager() {
		return api.getPartyRoleManager();
    }

    public static ObjectIdentity getObjectIdentity(Object domainObject) {
        return api.getObjectIdentity(domainObject);
    }

    public static SecurityIdentity getSecurityIdentity(Object authority) {
		return api.getSecurityIdentity(authority);
    }

	public static AuthorizationHash getAuthorizationHash(Object authority) {
		return api.getAuthorizationHash(authority);
	}

    /**
     * Optionally returns a string value of party identifier.
     */
    public static Optional<String> getPartyIdentifier() {
        return api.getPartyIdentifier();
    }

    /**
     * Retrieves the Party by an identity of the currently accessible Subject
     */
    public static Party getCurrentParty() {
        return api.getCurrentParty().orElse(null);
    }

	/**
	 * Returns the first PasswordMatcher occurrence found within the realms of
	 * the currently accessible SecurityManager.
	 */
	public static PasswordMatcher getPasswordMatcher() throws RuntimeException {
		return api.getPasswordMatcher();
	}

	/**
	 * Returns the first PasswordService occurance found within the realms of
	 * the currently accessible SecurityManager.
	 */
	public static PasswordService getPasswordService() throws RuntimeException {
		return api.getPasswordService();
	}

	/**
	 * Authenticate current subject by the given party's principals.
	 *
	 * The calling code must manually unbind the authenticated subject from the
	 * current thread by calling PlayAuth.unbind(), unless it is executing inside
	 * the scope of ch.insign.playauth.controllers.actions.WithSubjectAction.
	 */
    public static void authenticate(Party party, Http.Context ctx) {
        api.authenticate(party, ctx);
    }

	public static void authenticate(Party party) {
    	api.authenticate(party);
	}

	/**
	 * Binds a subject to the current thread.
	 */
	public static void bind(Http.Context ctx) {
		api.bind(ctx);
	}

	/**
	 * Removes a subject from current thread.
	 */
	public static void unbind() {
		api.unbind();
	}

    /**
     * Allows this Party to 'run as' or 'assume' another identity indefinitely.
     */
    public static void impersonate(Party party) throws NullPointerException, IllegalStateException {
        api.impersonate(party);
    }

    /**
     * Returns {@code true} if this {@code Party} is 'impersonating' another identity other than its original one or
     * {@code false} otherwise (normal {@code Party} state).  See the {@link #impersonate impersonate} method for more
     * information.
     */
    public static boolean isImpersonated() {
        return api.isImpersonated();
    }

    /**
     * Releases the current 'impersonate' (assumed) identity and reverts back to the previous 'pre impersonate'
     * identity that existed before {@code #impersonate impersonate} was called.
     */
    public static Party endImpersonation() {
	    return api.endImpersonation().orElse(null);
    }

    /**
     * Returns the previous 'pre impersonate' identity of this {@code Party} before assuming the current
     * {@link #impersonate impersonate} identity, or {@code null} if this {@code Party} is not operating under an assumed
     * identity (normal state).
     */
    public static Party getPreviousParty() {
        return api.getPreviousParty().orElse(null);
    }

    public static void login(AuthenticationToken token) throws AuthenticationException {
        api.login(token);
    }

    public static void logout() {
        api.logout();
    }

    public static boolean isAuthenticated() {
        return api.isAuthenticated();
    }

    public static boolean isRemembered() {
        return api.isRemembered();
    }

    public static boolean isAnonymous() {
        return api.isAnonymous();
    }

	/**
	 * Returns {@code true} if anonymous party is granted the given permission
	 */
	public static boolean isRestricted(DomainPermission<?> permission) {
		return api.isRestricted(permission);
	}

	/**
	 * Returns {@code true} if anonymous party is granted the given permission on all objects of the given type.
	 */
	public static <T, V extends T> boolean isRestricted(DomainPermission<T> permission, Class<V> target) {
		return api.isRestricted(permission, target);
	}

	/**
	 * Returns {@code true} if anonymous party is granted the given permission on the given object.
	 */
	public static <T, V extends T> boolean isRestricted(DomainPermission<T> permission, V target) {
		return api.isRestricted(permission, target);
	}

    /**
     * Returns {@code true} if current party is granted the given permission.
     */
    public static boolean isPermitted(DomainPermission<?> permission) {
        return api.isPermitted(permission);
    }

	/**
	 * Returns {@code true} if current party is granted the given permission on all objects of the given type.
	 */
	public static <T, V extends T> boolean isPermitted(DomainPermission<T> permission, Class<V> target) {
		return api.isPermitted(permission, target);
	}

    /**
     * Returns {@code true} if current party is granted the given permission on the given object.
     */
    public static <T, V extends T> boolean isPermitted(DomainPermission<T> permission, V target) {
	    return api.isPermitted(permission, target);
    }

    /**
     * Throws {@code AuthorizationException} if current party is NOT granted the given permission.
     * @throws AuthorizationException
     */
    public static void requirePermission(DomainPermission<?> permission) throws AuthorizationException {
	    api.requirePermission(permission);
    }

	/**
	 * Throws {@code AuthorizationException} if current party is NOT granted the given permission on all objects of the given type.
	 * @throws AuthorizationException
	 */
	public static <T, V extends T> void requirePermission(DomainPermission<T> permission, Class<V> target) throws AuthorizationException {
		api.requirePermission(permission, target);
	}

    /**
     * Throws {@code AuthorizationException} if current party is NOT granted the given permission on the given object.
     * @throws AuthorizationException
     */
    public static <T, V extends T> void requirePermission(DomainPermission<T> permission, V target) throws AuthorizationException {
	    api.requirePermission(permission, target);
    }


    public static <T> T execute(Http.Context ctx, final Supplier<T> block) {
	    return api.execute(ctx, block);
    }

	public static void execute(Http.Context ctx, final Runnable block) {
		api.execute(ctx, block);
	}

    public static <T> T executeAs(Party party, final Supplier<T> block) {
	    return api.executeAs(party, block);
    }

	public static void executeAs(Party party, final Runnable block) {
		api.executeAs(party, block);
	}

    public static <T> T executeWithTransaction(Http.Context ctx, final Supplier<T> block) {
        return api.executeWithTransaction(ctx, block);
    }

	public static void executeWithTransaction(Http.Context ctx, final Runnable block) {
		api.executeWithTransaction(ctx, block);
	}

    public static <T> T executeWithTransactionAs(Party party, final Supplier<T> block) {
        return api.executeWithTransactionAs(party, block);
    }

	public static void executeWithTransactionAs(Party party, final Runnable block) {
		api.executeWithTransactionAs(party, block);
	}
}
