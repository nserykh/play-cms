/**
 * Justboil.me - a TinyMCE image upload plugin
 * jbimages/js/dialog-v4.js
 *
 * Released under Creative Commons Attribution 3.0 Unported License
 *
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: http://justboil.me/
 * Author: Viktor Kuzhelnyi
 *
 * Version: 2.3 released 23/06/2013
 */
 
var jbImagesStandalone = {
    inputId : "",

    uploadFinish : function(result) {

        window.clearTimeout(this.timeoutStore);
        document.getElementById("upload_in_progress").style.display = 'none';
        document.getElementById("upload_infobar").style.display = 'block';
        document.getElementById("upload_infobar").innerHTML = result.result;
        document.getElementById("upload_form_container").style.display = 'block';

		if (result.resultCode == 'failed')
		{

		}
		else
		{
			document.getElementById("upload_in_progress").style.display = 'none';
			document.getElementById("upload_infobar").style.display = 'block';
			document.getElementById("upload_infobar").innerHTML = 'Upload Complete';

            window.parent.window.jbImagesStandalone.updateInput(result);

		}
	},

    browse : function(result) {
        this.inputId = result.inputId;
        jQuery("#imageUploadModal").show();
    },

    updateInput : function(result) {
        document.getElementById(this.inputId).value = result.filename;
        jQuery("#imageUploadModal").hide();
    }

};