/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.filter;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This meta filter provides filtering for filter tags, which in turn other filters can use.
 *
 * @author bachi
 */
public class TagContentFilter extends ContentFilter  {
	private final static Logger logger = LoggerFactory.getLogger(TagContentFilter.class);

    /** Filters tags of the form: [[tag text]] **/
    static final Pattern pattern = Pattern.compile("\\[\\[(.*?)\\]\\]");

    @Override
    public String[] filterTags() {
        return null;
    }

    @Override
    public String processInput(String input, Filterable source) {
        return process(input, FilterManager.EventType.INPUT, source);
    }

    @Override
    public String processOutput(String output, Filterable source) {
        return process(output, FilterManager.EventType.OUTPUT, source);
    }

    /**
     * Search for filter tags in the source content. If found, have the FilterManager process these tags and
     * replace the tags with the filter results.
     *
     * @param content the source content containing tags to search for
     * @param type input or output event
     * @return the processed source content
     */
    protected String process(String content, FilterManager.EventType type, Filterable source) {
        Matcher m = pattern.matcher(content);
        StringBuffer result = new StringBuffer();

        while (m.find()) {
            logger.debug("Processing tag: " + m.group(1));
            String replacement = getFilterManager().processTag(m.group(1), type, source);
            if (replacement != null) {
                m.appendReplacement(result, replacement);
            }
        }
        m.appendTail(result);

        return result.toString();
    }
}
