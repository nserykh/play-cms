/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.i18n.Language;

/**
 * A basic query data object, suitable for default queries.
 * Extend it for specialized searches.
 *
 * Note: The default result limit is set to 100
 */
public class SearchQuery  {
	private final static Logger logger = LoggerFactory.getLogger(SearchQuery.class);

    public String query;
    public int limit = 100;
    public int page;
    public String language = Language.getCurrentLanguage();

    public SearchQuery(String query) {
        this.query = query;

    }

    public SearchQuery(String query, int limit, int page, String language) {
        this.query = query;
        this.limit = limit;
        this.page = page;
        this.language = language;
    }

    public String toString() {
        return String.format("SearchQuery(query: '%s')", query);
    }
}
