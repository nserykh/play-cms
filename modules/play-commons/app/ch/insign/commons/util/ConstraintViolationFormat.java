/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import play.db.jpa.JPA;
import com.google.common.base.Throwables;
import play.i18n.Messages;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

public final class ConstraintViolationFormat {
	private ConstraintViolationFormat() { /* no instances */ }

	public static Optional<String> formatIfConstraintViolationException(Throwable throwable) {
		return Optional.of(throwable)
				.filter(t -> t instanceof ConstraintViolationException)
				.map(Optional::of)
				.orElseGet(() -> Optional.of(throwable)
						.map(Throwables::getRootCause)
						.filter(t -> t instanceof ConstraintViolationException))
				.map(t -> (ConstraintViolationException) t)
				.map(ConstraintViolationFormat::format);
	}

	public static String format(ConstraintViolationException e) {
		return e.getConstraintViolations()
				.stream()
				.map(ConstraintViolationFormat::format)
				.reduce("ConstraintViolation(s):", (r, a) -> r + "\n\t" + a);
	}

	public static String format(ConstraintViolation v) {
		String beanId;
		try {
			beanId = JPA.withTransaction(() -> Optional.ofNullable(v.getRootBean())
					.map(o -> JPA.em().getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(o))
					.map(String::valueOf)
					.orElse("?"));
		} catch (Throwable t) {
			throw Throwables.propagate(t);
		}

		return String.format("%s(%s).%s=%s; %s",
				v.getRootBeanClass().getName(),
				beanId,
				v.getPropertyPath(),
				v.getInvalidValue(),
				Messages.get(v.getMessage()));
	}
}
